/*
Nama  : Risma Febrian
Nim   : 301230011
Kelas : 1B
Prodi : Teknik Informatika
*/
#include <iostream>
using namespace std;

int faktorial(int X){
    int i=1, f; f=1;
while(i<=X)
 {
    f=f*i; i++;
 }
return(f);
}

int main(){
    int n, r, a, permutasi, kombinasi;
char h;
cout << "PERMUTASI DAN KOMBINASI" <<endl;
cout << "=======================" <<endl <<endl;
cout << "Pilih Permutasi atau Kombinasi ? (p/k) : ";
cin >>h;
cout <<endl;
cout << "Tentukan nilai n : ";
cin >>n;
cout << "Tentukan nilai r : ";
cin >>r;
cout <<endl;
a=n-r;
switch (h)
{
case 'p':
permutasi = faktorial(n) / faktorial(a);
cout << "Hasil permutasi dari " <<n<< " dan " <<r<< " adalah " <<permutasi <<endl;
break;

case 'k':
kombinasi = faktorial(n) / (faktorial(a) * faktorial(r));
cout << "Hasil kombinasi dari " <<n<< " dan " <<r<< " adalah " <<kombinasi <<endl;
break;
    }
}
